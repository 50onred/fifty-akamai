import json
import os
import requests
from ConfigParser import SafeConfigParser

class Actions(object):
    INVALIDATE = 'invalidate'
    REMOVE = 'remove'

class Domain(object):
    PRODUCTION = 'production'
    STAGING = 'staging'

class AkamaiApi(object):
    base_url = 'https://api.ccu.akamai.com{}'
    queue_url = base_url.format('/ccu/v2/queues/default')

    def __init__(self, username=None, password=None, profile=None):
        if profile:
            # Let ConfigParser exceptions bubble up when missing keys or section
            config = SafeConfigParser()
            config.read([os.path.expanduser('~/.akamai/credentials')])
            username = config.get(profile, 'username')
            password = config.get(profile, 'password')

        self.username = username
        self.password = password
        self.session = requests.Session()
        self.session.auth = (self.username, self.password)
        self.session.headers['Content-Type'] = 'application/json'


    def purge_arls(self, arls, domain=Domain.PRODUCTION, action=Actions.INVALIDATE):
        """Purges arl(s).
        Returns a requests :class:`Response <Response>` object.

        :param arls: a single akamai arl or list of arls to be purged.
        :param domain: one of the :class:`Domain` options PRODUCTION or STAGING.
        :param action: one of the :class:`Action` actions.

        The response will be JSON data similar to:

        {
          "httpStatus" : 201,
          "detail" : "Request accepted."
          "estimatedSeconds" : 420,
          "purgeId" : "95b5a092-043f-4af0-843f-aaf0043faaf0",
          "progressUri" : "/ccu/v2/purges/95b5a092-043f-4af0-843f-aaf0043faaf0",
          "pingAfterSeconds" : 420,
          "supportId" : "17PY1321286429616716-211907680",
        }
        """

        return self._purge(arls, 'arl', domain, action)

    def purge_cpcode(self, cpcodes, domain=Domain.PRODUCTION,
                     action=Actions.INVALIDATE):
        """Purges cpcode(s).
        Returns a requests :class:`Response <Response>` object.

        :param cpcodes: a single cpcode or list of cpcodes to be purged.
        :param domain: one of the :class:`Domain` options PRODUCTION or STAGING.
        :param action: one of the :class:`Action` actions.

        The response will be JSON data similar to:

        {
          "httpStatus" : 201,
          "detail" : "Request accepted."
          "estimatedSeconds" : 420,
          "purgeId" : "95b5a092-043f-4af0-843f-aaf0043faaf0",
          "progressUri" : "/ccu/v2/purges/95b5a092-043f-4af0-843f-aaf0043faaf0",
          "pingAfterSeconds" : 420,
          "supportId" : "17PY1321286429616716-211907680",
        }
        """

        return self._purge(cpcodes, 'cpcode', domain, action)


    def _purge(self, objects, object_type, domain, action):
        if isinstance(objects, (str, unicode)):
            objects = [objects]
        data = {
            'domain': domain,
            'action': action,
            'type': object_type,
            'objects': objects,
        }
        return self.session.post(self.queue_url,
                             data=json.dumps(data))

    def purge_status(self, progress_uri):
        """Checks the status of a previous purge.
        :param progress_uri: A progress_uri from the response of a previous
        purge.

        Response JSON will have data
        similar to:

        {
          "httpStatus" : 200,
          "purgeId" : "95b5a092-043f-4af0-843f-aaf0043faaf0",
          "requestStatus" : "Done"
          "submittedBy" : "test1",
          "submissionTime" : "2011-11-14T16:01:24Z",
          "completionTime" : "2011-11-14T16:05:34Z",
          "supportId" : "17SY1321286746647733-211907680",
        }

        """
        return self.session.get(self.base_url.format(progress_uri))

    def queue_length(self):
        """Gets the queue length and returns the resulting JSON
        """
        return self.session.get(self.queue_url)

