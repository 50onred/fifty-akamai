from collections import defaultdict
from fiftyakamai.resource import AkamaiResource
from urlparse import urlparse


class ContentControlResource(AkamaiResource):
    endpoint = '/ccu/v3/invalidate/url/'

    def invalidate_objects(self, hostname, objects, chunk=0):
        # If you specify chunk, you may receive more than one result
        i = 0
        items = objects[i:i + chunk] if chunk else objects
        while items:
            yield self.post(json={
                'hostname': hostname,
                'objects': items
            })

            i += chunk if chunk else len(objects)
            items = objects[i:i + chunk]

    def invalidate_url(self, url):
        parsed = urlparse(url)
        return next(self.invalidate_objects(parsed.netloc, [parsed.path]))

    def invalidate_urls(self, urls, chunk=0):
        # If you specify more than one host or chunk, you may receive more than one result
        objects_to_invalidate = defaultdict(set)

        for url in urls:
            parsed = urlparse(url)
            objects_to_invalidate[parsed.netloc].add(parsed.path)

        return [
            (hostname, chunk_items)
            for hostname, objects in objects_to_invalidate.iteritems()
            for chunk_items in self.invalidate_objects(hostname, list(objects), chunk)
        ]
