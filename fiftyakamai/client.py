import os
import requests
from ConfigParser import SafeConfigParser
from akamai.edgegrid import EdgeGridAuth
from urllib import urlencode
from urlparse import urljoin


class AkamaiClient(object):
    def __init__(self, base_url=None, client_token=None, client_secret=None, access_token=None,
            profile=None):
        if profile:
            # Let ConfigParser exceptions bubble up when missing keys or section
            config = SafeConfigParser()
            config.read([os.path.expanduser('~/.akamai/credentials')])

            access_token = config.get(profile, 'access_token')
            base_url = config.get(profile, 'base_url')
            client_secret = config.get(profile, 'client_secret')
            client_token = config.get(profile, 'client_token')

        self.base_url = base_url
        self.session = requests.Session()
        self.session.auth = EdgeGridAuth(client_token=client_token, client_secret=client_secret,
                access_token=access_token)

    def request(self, method, endpoint, path=None, query=None, **kwargs):
        url = urljoin(self.base_url, endpoint)

        if path:
            url = urljoin(url, unicode(path))

        if query:
            query = {key: value for key, value in query.iteritems() if value is not None}
            if query:
                url += '?{}'.format(urlencode(query))

        response = self.session.request(method, url, **kwargs)
        response.raise_for_status()

        return response
