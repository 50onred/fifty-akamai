from StringIO import StringIO
from xml.etree.ElementTree import Element, ElementTree

from zeep import Client
from zeep.transports import Transport


def _build_eccu_request(directory):
    eccu = Element('eccu')
    revalidate = Element('revalidate')
    revalidate.text = 'now'

    if directory.strip() == '/':
        eccu.append(revalidate)
    else:
        match = Element('match:recursive-dirs', value=directory.replace('/', ''))
        match.append(revalidate)
        eccu.append(match)

    out = StringIO()
    ElementTree(eccu).write(out, encoding='utf-8', xml_declaration=True)
    return out.getvalue()


class EnhancedContentControlResource(object):
    wsdl_url = 'https://control.akamai.com/webservices/services/PublishECCU?wsdl'
    property_type = 'hostheader'
    strict_host_match = True

    def __init__(self, username, password):
        self.username = username
        self.password = password

    def revalidate_directory(self, host, directory, notification_email, filename='', notes='', version=''):
        client = Client(self.wsdl_url, transport=Transport(http_auth=(self.username, self.password)))
        client.service.upload(filename, _build_eccu_request(directory), notes, version, host, self.property_type,
                              self.strict_host_match, notification_email)

    def revalidate_directories(self, host, directories, notification_email, filename='', notes='', version=''):
        self.revalidate_directory(host, ' '.join(directories), notification_email, filename, notes, version)
