from .resource import AkamaiResource


class GroupsResource(AkamaiResource):
    endpoint = '/papi/v0/groups/'


class BasePropertiesResource(AkamaiResource):
    def __init__(self, client, contract_id, group_id):
        super(BasePropertiesResource, self).__init__(client)
        self.contract_id = contract_id
        self.group_id = group_id

    def get_default_query(self):
        args = super(BasePropertiesResource, self).get_default_query()
        args.update({
            'contractId': self.contract_id,
            'groupId': self.group_id
            })
        return args


class EdgeHostnamesResource(BasePropertiesResource):
    endpoint = '/papi/v0/edgehostnames/'

    def fetch(self, options=None):
        extra_query = {'options': options} if options else None
        return super(EdgeHostnamesResource, self).fetch(extra_query=extra_query)


class PropertiesResource(BasePropertiesResource):
    endpoint = '/papi/v0/properties/'


class PropertyVersionsResource(BasePropertiesResource):
    endpoint = '/papi/v0/properties/{propertyId}/versions/'

    def __init__(self, client, contract_id, group_id, property_id):
        super(PropertyVersionsResource, self).__init__(client, contract_id, group_id)
        self.property_id = property_id

    def get_endpoint(self):
        return self.endpoint.format(propertyId=self.property_id)

    def fetch_latest_version(self, activated_on=None):
        return self.fetch(path='latest/', activatedOn=activated_on)

    def fetch_version(self, version):
        return self.fetch(path=version)

    def fetch_rule_tree(self, version):
        return self.fetch(path='{}/rules/'.format(version))

    def put_rule_tree(self, version, rules, etag):
        headers = {
                'Content-Type': 'application/vnd.akamai.papirules.latest+json',
                'If-Match': '"{}"'.format(etag.strip('"'))
                }
        return self.put(path='{}/rules/'.format(version), json={'rules': rules}, headers=headers)
