class AkamaiResource(object):
    endpoint = None

    def __init__(self, client):
        self.client = client

    def get_endpoint(self):
        return self.endpoint

    def get_default_query(self):
        return {}

    def fetch(self, path=None, extra_query=None, **kwargs):
        return self.request('GET', path=path, extra_query=extra_query, **kwargs)

    def put(self, path=None, extra_query=None, **kwargs):
        return self.request('PUT', path=path, extra_query=extra_query, **kwargs)

    def post(self, path=None, extra_query=None, **kwargs):
        return self.request('POST', path=path, extra_query=extra_query, **kwargs)

    def request(self, method, path=None, extra_query=None, **kwargs):
        query = self.get_default_query()
        if extra_query:
            query.update(extra_query)
        return self.client.request(method, self.get_endpoint(), path=path, query=query, **kwargs)
