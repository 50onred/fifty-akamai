#!/usr/bin/env python
try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

setup(
    name='fiftyakamai',
    version='3.1.1',
    description='Akamai API',
    long_description='Exposes the Akamai REST API as a Python class',
    author='50onRed',
    author_email='dev@50onred.com',
    url='https://bitbucket.org/50onred/fifty-akamai',
    packages=['fiftyakamai'],
    package_dir={'fiftyakamai': 'fiftyakamai'},
    include_package_data=True,
    install_requires=['edgegrid-python==1.0.10', 'zeep==0.13.0'],
    license='Copyright 50onRed',
    zip_safe=False,
    classifiers=(
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        'License :: OSI Approved :: Copyright 50onRed',
        'Programming Language :: Python',
    ),
)
